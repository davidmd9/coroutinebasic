package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.util.concurrent.atomic.AtomicLong
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCheckUI.setOnClickListener{
            Toast.makeText(this, "UI AVAILABLE", Toast.LENGTH_SHORT).show()
        }




//  coroutine 1
//        button.setOnClickListener{
//            println("rnr Start")
//            GlobalScope.launch {
//                delay(1000)
//                println("rnr Hello")
//            }
//            Thread.sleep(2000) // wait for 2 seconds
//            println("rnr Stop")
//        }

// потоки
//        button.setOnClickListener{
//            val c = AtomicLong()
//
//            for (i in 1..100000L)
//                thread(start = true) {
//                    c.addAndGet(i)
//                }
//
//            println(c.get())
//        }

// подпрограммы
//        button.setOnClickListener{
//            val c = AtomicLong()
//
//            for (i in 1..100000L)
//                GlobalScope.launch {
//                    c.addAndGet(i)
//                }
//
//            println(c.get())
//        }



//        Callback way
//        combineResponce(object : DoneListener{
//            override fun onDone(s: String) {
//                textView.text = s
//            }
//        })


//        coroutine way
//        GlobalScope.launch {
//            val job = async(Dispatchers.IO) {
//                val s = combineResponces()
//                print("$s")
//                textView.text = s
//            }
//            job.join()
//        }

//        runBlocking { // this: CoroutineScope
//            launch {
//                delay(2000)
//                println("Task from runBlocking")
//            }
//
//            coroutineScope { // Creates a coroutine scope
//                launch {
//                    delay(5000)
//                    println("Task from nested launch")
//                }
//
//                delay(1000)
//                println("Task from coroutine scope") // This line will be printed before the nested launch
//            }
//
//            println("Coroutine scope is over") // This line is not printed until the nested launch completes
//        }

    }

    interface DoneListener {
        fun onDone(s: String)
    }

    fun getInfoFromServer(l : DoneListener){
        Thread{
            Thread.sleep(5000)
            l.onDone("Some server info")
        }.start()
    }

    fun getInfoFromDB(l : DoneListener){
        Thread{
            Thread.sleep(500)
            l.onDone("Some DB info")
        }.start()
    }

    fun combineResponce(l : DoneListener){
        var resCount = 0;
        var resStr = " "
        getInfoFromDB(object:DoneListener{
            override fun onDone(s: String) {
                resCount++
                if (resCount == 2) {
                    l.onDone(resStr+s)
                } else {
                    resStr = resStr+s
                }
            }
        })
        getInfoFromServer(object:DoneListener{
            override fun onDone(s: String) {
                resCount++
                if (resCount == 2) {
                    l.onDone(s+resStr)
                } else {
                    resStr = s+resStr
                }
            }
        })
    }

    suspend fun getInfoFromServer():String{
        delay(5000)
        return "Some server information"
    }

    suspend fun getInfoFromDB():String{
        delay(1500)
        return "some db information"
    }

    suspend fun combineResponces() : String {
        val serverData = GlobalScope.async {
            getInfoFromServer()
        }
        var dbData = GlobalScope.async {
            getInfoFromDB()
        }
        var result = GlobalScope.async {
            serverData.await()+ " " +dbData.await()
        }
        return result.await();
    }



}
